package com.br.semdesperdicios.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import com.br.semdesperdicios.MainActivity;

import com.br.semdesperdicios.R;
import com.br.semdesperdicios.utils.GPSTracker;

/**
 * Created by Jessica Pires on 6/23/2015.
 */
public class MapsResumeFragment extends Fragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private GoogleMap mMap;
    GPSTracker gps = null;
    public static int TYPE_DESPERDICIO = 0;
    public static int TYPE_VAZAMENTO = 1;
    public static int TYPE_OUTROS = 2;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MapsResumeFragment newInstance(int sectionNumber) {
        MapsResumeFragment fragment = new MapsResumeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public MapsResumeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_map_resume, container, false);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    private void setUpMap() {

        gps = new GPSTracker(getActivity().getApplicationContext());

        if (gps.canGetLocation()) {

            mMap.addMarker(new MarkerOptions().position(
                    new LatLng(gps.getLatitude(),
                            gps.getLongitude()))
                    .title("Sua localização atual.")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(gps.getLatitude(), gps.getLongitude()), 13.0f));


            ParseQuery<ParseObject> query = ParseQuery.getQuery("Marker");
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {

                        for (ParseObject objMaker : scoreList) {

                            String boxMessage = "";
                            String desMessage = "";

                            if (objMaker.getInt("type") == TYPE_DESPERDICIO)
                                boxMessage = "Desperdício";
                            else if (objMaker.getInt("type") == TYPE_VAZAMENTO)
                                boxMessage = "Vazamento";
                            else
                                boxMessage = "Outros";

                            if (objMaker.getBoolean("showComment")) {
                                desMessage = objMaker.getString("comment");
                            }

                            mMap.addMarker(new MarkerOptions().position(new LatLng(
                                            objMaker.getDouble("lat"),
                                            objMaker.getDouble("long"))
                            ).title(boxMessage).snippet(desMessage));
                        }

                        Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    } else {
                         Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });

        } else {
            gps.showSettingsAlert();
        }

    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();

            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }
}