package com.br.semdesperdicios.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.br.semdesperdicios.MainActivity;

import com.br.semdesperdicios.R;

/**
 * Created by Jessica Pires on 6/23/2015.
 */
public class PreferenciasFragment extends Fragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PreferenciasFragment newInstance(int sectionNumber) {
        PreferenciasFragment fragment = new PreferenciasFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PreferenciasFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_preferencias, container, false);
        Spinner spinnerSaneamento = (Spinner) rootView.findViewById(R.id.spinnerSaneamento);
         ArrayAdapter<CharSequence> adapterSaneamento = ArrayAdapter.createFromResource(getActivity(),
                R.array.companhiasSaneamento, android.R.layout.simple_spinner_item);
        adapterSaneamento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerSaneamento.setAdapter(adapterSaneamento);

        Spinner spinnerEnergia = (Spinner) rootView.findViewById(R.id.spinnerEnergia);
        ArrayAdapter<CharSequence> adapterEnergia = ArrayAdapter.createFromResource(getActivity(),
                R.array.companhiasEnergia, android.R.layout.simple_spinner_item);
        adapterEnergia.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerEnergia.setAdapter(adapterEnergia);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}
