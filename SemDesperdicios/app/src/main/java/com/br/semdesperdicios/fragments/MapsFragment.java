package com.br.semdesperdicios.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.MapsInitializer;
import com.parse.Parse;
import com.parse.ParseObject;

import com.br.semdesperdicios.MainActivity;

import com.br.semdesperdicios.R;
import com.br.semdesperdicios.utils.GPSTracker;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Jessica Pires on 6/23/2015.
 */
public class MapsFragment extends Fragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    Button btnDenunciar = null;
    EditText edtComment = null;
    GPSTracker gps = null;

    public static int TYPE_DESPERDICIO = 0;
    public static int TYPE_VAZAMENTO = 1;
    public static int TYPE_OUTROS = 2;
    public int mSelect = 0;
    public static boolean model = false;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MapsFragment newInstance(int sectionNumber) {
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public void setModel(){

        if(!model) {
            Parse.enableLocalDatastore(getActivity().getApplicationContext());
            Parse.initialize(getActivity().getApplicationContext(), "FSJHNukG8qn65k9homZu64mzDejbCdumYOqg9pVL", "ln0MWqvMJRuwMEMTXNj5mY9trrncEze3FXEo5wc9");
            model = true;
        }
    }


    public void denunciar(){

        ParseObject objMarker = new ParseObject("Marker");
        objMarker.put("long", gps.getLongitude());
        objMarker.put("lat", gps.getLatitude());
        objMarker.put("comment", edtComment.getText().toString());
        objMarker.put("type", mSelect);
        objMarker.put("showComment", false);
        objMarker.saveInBackground();

        Toast.makeText(getActivity().getApplicationContext(), "Notificação registrada com sucesso!", Toast.LENGTH_LONG).show();

        Fragment fragment = new MapsResumeFragment().newInstance(1);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();

    }

    protected void dialog() {

        String[] items = new String[]{"Desperdício", "Vazamento", "Outros"};

        Dialog dia = new AlertDialog.Builder(getActivity())
                //.setIcon(R.drawable.icon)
                .setTitle("O que está acontecendo?")
                .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mSelect = which;
                    }
                })
                .setPositiveButton("Notificar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                denunciar();

                            }
                        })
                .setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        })
                .create();
        dia.show();

    }


    private void setUpMapIfNeeded() {

        if (mMap == null) {
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
        }

        setUpMap();

    }

    private void setUpMap() {

        gps = new GPSTracker(getActivity());

        if(gps.canGetLocation()){

            mMap.clear();

            mMap.addMarker(new MarkerOptions().position(
                    new LatLng(gps.getLatitude(),
                            gps.getLongitude()))
                    .title("Sua localização atual.")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(gps.getLatitude(), gps.getLongitude()), 19.0f));

        }else{

            gps.showSettingsAlert();

        }
    }

    public MapsFragment() {

    }


    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MapsInitializer.initialize(getActivity());
        rootView = inflater.inflate(R.layout.fragment_map, container, false);
        setModel();
        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }


    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();

        btnDenunciar = (Button) rootView.findViewById(R.id.denunciar);
        edtComment = (EditText) rootView.findViewById(R.id.comment);
        btnDenunciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog();

            }

        });

    }

}
