package com.br.semdesperdicios;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;

import com.br.semdesperdicios.fragments.AjudaFragment;
import com.br.semdesperdicios.fragments.MapsFragment;
import com.br.semdesperdicios.fragments.MapsResumeFragment;
import com.br.semdesperdicios.fragments.PreferenciasFragment;
import com.br.semdesperdicios.fragments.SobreFragment;
import com.br.semdesperdicios.R;
import com.br.semdesperdicios.fragments.PoliticaPrivacidadeFragment;
import com.br.semdesperdicios.fragments.TermosFragment;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {


        Fragment fragment = null;

        switch(position){
            case 0:
                fragment = new MapsFragment().newInstance(position+1);
                break;
            case 1:
                fragment = new MapsResumeFragment().newInstance(position+1);
                break;

            case 2:
                fragment = null;//new MapsResumeFragment().newInstance(position +1);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Eu notifico os desperdícios de água. Notifique você também!");
                intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.br.semdesperdicios");
                startActivity(Intent.createChooser(intent, "Compartilhe essa idéia!"));
                break;
            case 3:
                fragment = new PreferenciasFragment().newInstance(position+1);
                break;
            case 4:
                fragment = new SobreFragment().newInstance(position+1);
                break;
            case 5:
                fragment = new PoliticaPrivacidadeFragment().newInstance(position+1);
                break;
            case 6:
                fragment = new TermosFragment().newInstance(position+1);
                break;
            case 7:
                fragment = new AjudaFragment().newInstance(position+1);
                break;
            case 8:
                finish();
                break;
            default:
                fragment = new MapsFragment().newInstance(position+1);
                break;

        }
        // update the main content by replacing fragments
        if(fragment != null){

            onSectionAttached(position);
            restoreActionBar();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
        }


    }

    public void onSectionAttached(int number) {
        String[] stringArray = getResources().getStringArray(R.array.section_titles);
        if (number >= 0) {
            mTitle = stringArray[number];
        }

    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);

    }

}
